from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name="home"),
    path('about/', views.about, name="about"),
    path('result/<str:task_id>/', views.check_result, name="check_result"),
    path('students/', views.students, name="students"),
    path('send-email/', views.send_email_view, name="send_email")
]