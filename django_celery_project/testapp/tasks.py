from celery import shared_task
from time import sleep

import pandas as pd
from .models import Student
import uuid

from celery import shared_task
from django.core.mail import send_mail

@shared_task
def send_email_task(subject, message, from_email, recipient_list):
    send_mail(subject, message, from_email, recipient_list)

# @shared_task
# def export_student_excel():
#     objs = Student.objects.all()
#     payload = []
#     for obj in objs:
#         payload.append({
#             'name':obj.name,
#             'age':obj.age
#         })
    
#     df = pd.DataFrame(payload)
#     df.to_csv(f"{uuid.uuid4()}.csv", encoding="UTF-8")



# @shared_task(bind=True)
# def test_func(self):
#     for i in range(10):
#         print(i)
#     return "done"


# @shared_task
# def sub(x,y):
#     sleep(10)
#     return x - y