from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django_celery_project.celery import add
from celery.result import AsyncResult
from django.core.cache import cache
from .models import Student

def students(request):
    payload = []
    db = None
    if cache.get('students'):
        payload = cache.get('students')
        db = 'redis'
        print(cache.ttl('students'))
    else:
        objs = Student.objects.all()
        for obj in objs:
            payload.append(obj.name)
        db = 'sqlite'
        cache.set('students',payload, timeout=10)
    
    return JsonResponse({'status':200, 'db':db, 'data':payload})

# Enqueue task using delay()
# def home(request):
#     print("Result: ")
#     result1 = add.delay(10,20)
#     print("resut 1:", result1)

#     result2 = sub.delay(50,30)
#     print("resut 2:", result2)
#     return render(request, 'home.html')

# Enqueue task using apply_async()
# def home(request):
#     print("Result: ")
#     result1 = add.apply_async(args=[10,20])
#     print("resut 1:", result1)

#     result2 = sub.apply_async(args=[50,30])
#     print("resut 2:", result2)
#     return render(request, 'home.html')


def home(request):
    # export_student_excel.delay()
    return render(request, 'home.html')

    # result = add.delay(10,30)
    # return render(request, 'home.html', {'result':result})


def check_result(request, task_id):
    result = AsyncResult(task_id)
    return render(request, 'result.html', {'result':result})


def about(request):
    return render(request, 'about.html')

# def test(request):
#     test_func.delay()
#     return HttpResponse("Done")




# myapp/views.py

from django.http import HttpResponse
from .tasks import send_email_task
from django.conf import settings

def send_email_view(request):
    subject = 'Test mail'
    message = 'This message is send using celery'
    from_email = settings.EMAIL_HOST_USER
    recipient_list = ['shaikhmuarif@gmail.com', 'muarifshaikh66@gmail.com']
    
    # Trigger the asynchronous task
    result = send_email_task.delay(subject, message, from_email, recipient_list)
    
    return render(request, 'home.html', {'result': result})
